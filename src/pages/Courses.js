import React from 'react'
import CourseCard from '../components/CourseCard'
import coursesData from '../data/coursesData'

export default function Courses() {
    return (
        <>
            <h1>Courses</h1>
            {coursesData.map(e => {
                return <CourseCard key={e.id} {...e} />
            })}
        </>
    )
}
